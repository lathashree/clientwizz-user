/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';

import { Router } from '@angular/router';
import { AuthService } from './auth-service';
import * as CONFIG from '../../config';
import * as firebase from 'firebase';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {
  auth: firebase.User | {};

  constructor(private analytics: AnalyticsService, private router: Router, private authService: AuthService) {
  }

  ngOnInit(): void {

    this.analytics.trackPageViews();
    firebase.initializeApp(   
       CONFIG.firebase.test
    
    );
    this.authService.auth()
      .then(user => this.auth = user)
      .catch(() => this.router.navigate(['/auth']));
  }
}
