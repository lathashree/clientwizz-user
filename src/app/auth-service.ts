import * as firebase from 'firebase';
import { Injectable } from '@angular/core';
import Swal from 'sweetalert2'
@Injectable()

export class AuthService {


    updatePassword(currentPassword: string, newPassword: string, confirmPassword: string) {
        const user = firebase.auth().currentUser;
        const creds = firebase.auth.EmailAuthProvider.credential(user.email, currentPassword);
        return user.reauthenticateAndRetrieveDataWithCredential(creds)
            .then(() => {
                user.updatePassword(newPassword)
                return firebase.auth().signOut()
            })
    }

    signUp(firstName: string, lastName: string, username: string, email: string, password: string, rePass: string) {
        return firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((createdUser) => {
                let user = firebase.auth().currentUser;
                user.updateProfile({
                    displayName: username,
                    photoURL: ''
                })
                const ref = firebase.database().ref("/users")
                ref.child(createdUser.user.uid).set({
                    uid: createdUser.user.uid,
                    email: email,
                    username: username,
                    firstName: firstName,
                    lastName: lastName,
                    creationTime: createdUser.user.metadata.creationTime,
                    report: ""
                })
            })
    }

    login(email: string, password: string) {
        return firebase.auth().signInWithEmailAndPassword(email, password)
            .then(() => this.authMeta())
    }

    private authMeta() {
        return new Promise((resolve, reject) => {
            const user: firebase.User = firebase.auth().currentUser
            const ref = firebase.database().ref("/users")
            return ref.orderByKey().equalTo(user.uid).once('value', function (userByIdSnapshot) {
                let userByID = userByIdSnapshot.val()
                if (!userByID) {
                    return reject("User is deleted")
                } else {
                    ref.child(user.uid).update({
                        "lastSignInTime": user.metadata.lastSignInTime
                    })
                    resolve()
                }
            })
        })
    }

    public auth() {
        return new Promise((resolve, reject) => {
            firebase.auth().onAuthStateChanged(user => {
                if (user) {
                    // User is signed in.
                    // console.log("Auth: ", user)
                    return firebase.database().ref("/users/"+user.uid).once("value", userSnapshot=>{
                        resolve(userSnapshot.val()||user)
                    })
                }
                reject()
            });
        })
    }

    public logout() {

       return  Swal({
            title: 'Are you sure?',
            text: 'You will not be able to use any pages!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, log me out!',
            cancelButtonText: 'No, keep me In'
          }).then((result) => {
            if (result.value) {
                return firebase.auth().signOut()
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                return Promise.reject("error");
            }
          })


        // console.log("logout ")
        // if (window.confirm('Are you sure you want to logout?')) {
        //     return firebase.auth().signOut()
        //   } else {
        //     return Promise.reject();
        //   }
        // return firebase.auth().signOut()
}
}
