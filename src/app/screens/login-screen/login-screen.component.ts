import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../auth-service';

@Component({
  selector: 'ngx-login-screen',
  templateUrl: './login-screen.component.html',
  styleUrls: ['./login-screen.component.scss'],
})
export class LoginScreenComponent implements OnInit {

  flashMessage: string;
  constructor(private authservice: AuthService, private router: Router) {

  }


  ngOnInit() {
  }

  onSubmit(form: NgForm) {

    const email = form.value.email;
    const password = form.value.password;

    this.authservice.login(email, password)
      .then(() => {
        this.router.navigate(['/user/dashboard']);
      })
      .catch(err => {
        this.flashMessage = err;
      });

  }


}
