import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../auth-service';
@Component({
    selector: 'ngx-sign-up-screen',
    templateUrl: './sign-up-screen.component.html',
    styleUrls: ['./sign-up-screen.component.scss']
})
export class SignUpScreenComponent implements OnInit {

    constructor(private authService: AuthService, private router: Router) { }

    ngOnInit() {
    }

    onSubmit(form: NgForm) {
        const firstName = form.value.firstname;
        const lastName = form.value.lastname;
        const username = form.value.username;
        const email = form.value.email;
        const password = form.value.password;
        const cpassword = form.value.cpassword;
        // this.router.navigate(["/login"]);
        this.authService.signUp(firstName, lastName, username, email, password, cpassword)
            .then(() => {
                this.router.navigate(['/auth/login'])
            })
            .catch();
    }


}
