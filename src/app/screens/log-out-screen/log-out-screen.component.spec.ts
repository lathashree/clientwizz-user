import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogOutScreenComponent } from './log-out-screen.component';

describe('LogOutScreenComponent', () => {
  let component: LogOutScreenComponent;
  let fixture: ComponentFixture<LogOutScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogOutScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogOutScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
