import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth-service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-log-out-screen',
  templateUrl: './log-out-screen.component.html',
  styleUrls: ['./log-out-screen.component.scss']
})
export class LogOutScreenComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    console.log("logout component ")
    this.authService.logout()
    .then(()=>{
      this.router.navigate(['auth']);
    }).catch(() => {
      this.router.navigate(['/pages/dashboard']);
    })
  }

}
