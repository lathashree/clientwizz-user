import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-grid-a-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Profile Settings',
    icon: 'nb-gear',
    link: '/pages/forms/layouts',
  },
  {
    title: 'Manage Users',
    icon: 'nb-person',
    link: '/pages/tables/users-table',
  },
  {
    title: 'Download NMM data',
    icon: 'nb-bar-chart',
    link: '/pages/tables/transactions-table',
  },
  {
    title: 'Support System',
    icon: 'nb-gear',
    link: '/pages/token',
  },

  {
    title: 'Logout',
    icon: 'nb-power',
    // link: '/auth/logout',
  },
];
