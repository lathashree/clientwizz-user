import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NmmDashboardComponent } from './nmm-dashboard.component';

describe('NmmDashboardComponent', () => {
  let component: NmmDashboardComponent;
  let fixture: ComponentFixture<NmmDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NmmDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NmmDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
