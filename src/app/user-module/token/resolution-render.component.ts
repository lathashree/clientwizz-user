import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';

@Component({
  template: `
    {{renderValue}}
  `,
})
export class ResolutionRenderComponent implements ViewCell, OnInit {

  renderValue: string;

  @Input() value: any;
  @Input() rowData: any;

  ngOnInit() {
    let messages = Object.values(this.value);
    messages.reverse().some((message: any) => {
      if(message.author === 'admin'){
        this.renderValue = message.text
        return true
      }
    })
  }


}
