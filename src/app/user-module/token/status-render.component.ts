import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';

@Component({
  template: `
    <span class="text-danger" *ngIf="renderValue === 'Pending'">{{renderValue}}</span><span *ngIf="renderValue !== 'Pending'">{{renderValue}}</span>
  `,
})
export class StatusRenderComponent implements ViewCell, OnInit {

  renderValue: string;

  @Input() value: any;
  @Input() rowData: any;

  ngOnInit() {
    this.renderValue = this.value
    return true
  }


}
