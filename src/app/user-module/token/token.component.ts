import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import * as firebase from 'firebase';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth-service';
import { ResolutionRenderComponent } from './resolution-render.component';
import {UidRenderComponent} from './uid-render.component'
import {StatusRenderComponent} from "./status-render.component"

@Component({
  selector: 'ngx-token',
  templateUrl: './token.component.html',
  styleUrls: ['./token.component.scss']
})
export class TokenComponent implements OnInit {
  settings = {

    actions: {
      add : false,
      edit: false,
      delete: false,
      position : "right",
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },

    columns: {
       uid: {
        title: 'Ticket Number',
        type: 'custom',
        renderComponent: UidRenderComponent,
      },
      subject: {
        title: 'Subject',
        type: 'string',
      },
      messages: {
        title: 'Resolution Details',
        type: 'custom',
        renderComponent: ResolutionRenderComponent,
      },
      status: {
        title: 'Status',
        type: 'custom',
        renderComponent:  StatusRenderComponent,
      },
 
    },
  };

  
  
  source: LocalDataSource = new LocalDataSource();
  uid

  constructor(private router: ActivatedRoute, private route: Router, private authService: AuthService) { }

  ngOnInit() {

    let supportticket = []
    this.authService.auth()
    .then((user:any) => {
      this.uid = user.uid;
      console.log("MY Uid",this.uid.substring(0,6))
      const ref = firebase.database().ref("/users")
      ref.child(this.uid+"/supportTickets").once('value', supportticketSnapshot => {
        let object = supportticketSnapshot.val()
        for(const key in object){
          if (object.hasOwnProperty(key)){
            object[key].uid = key;
            supportticket.push(object[key]);
          }
        }         
          this.source.load(supportticket);
      });
    });   

  }

  rowSelected(event){
    this.route.navigate(["/user/token/" + event.data.uid ])
  }

}
