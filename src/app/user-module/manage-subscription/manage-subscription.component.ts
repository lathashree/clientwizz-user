import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../auth-service"
import * as firebase from 'firebase';

@Component({
  selector: 'ngx-manage-subscription',
  templateUrl: './manage-subscription.component.html',
  styleUrls: ['./manage-subscription.component.scss']
})
export class ManageSubscriptionComponent implements OnInit {

  user
  transactions
  constructor(private auth: AuthService) { }

  ngOnInit() { 

    this.auth.auth()
    .then((user:any) =>{
      let ref = firebase.database().ref('/users')
      ref.child(user.uid+'/transactions').once('value', userSnapshot => {
        this.transactions = Object.values(userSnapshot.val())
        this.transactions = this.transactions[0]
      })
    })

  }

}
