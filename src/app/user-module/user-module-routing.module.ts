import { RouterModule, Routes, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NgModule, Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { PagesComponent } from './user-module.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { TokenComponent } from './token/token.component';
import { TokenRequestComponent } from './token-request/token-request.component';
import { ViewTokenComponent } from './view-token/view-token.component';


import { Observable } from 'rxjs';
import { DownloadComponent } from './download/download.component';
import { NmmDashboardComponent } from './nmm-dashboard/nmm-dashboard.component';
import { ManageSubscriptionComponent } from './manage-subscription/manage-subscription.component';
import { DownloadSecurekeyComponent } from './download-securekey/download-securekey.component';

@Injectable()
export class AuthResolver implements Resolve<firebase.User> {
  constructor() {}
 
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any>|Promise<any>|any {
    return firebase.auth().currentUser;
  }
}

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  resolve: {
    auth: AuthResolver,
  },
  children: [
    {
      path: 'download',
      component: DownloadComponent,
    },
  {
    path: 'token',
    component: TokenComponent,
  },
  {
    path: 'token/new',
    component: TokenRequestComponent,
  },
  {
    path: 'token/:id',
    component: ViewTokenComponent,
  },
  {
    path: 'dashboard',
    component: NmmDashboardComponent,
  },
  {
    path: 'download-securekey/:id',
    component: DownloadSecurekeyComponent,
  },
   {
    path: 'iot-dashboard',
    component: DashboardComponent,
  }, {
    path: 'ui-features',
    loadChildren: './ui-features/ui-features.module#UserUiFeaturesModule',
  }, {
    path: 'components',
    loadChildren: './components/components.module#ComponentsModule',
  }, {
    path: 'maps',
    loadChildren: './maps/maps.module#MapsModule',
  }, {
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsModule',
  }, {
    path: 'editors',
    loadChildren: './editors/editors.module#EditorsModule',
  }, {
    path: 'forms',
    loadChildren: './forms/forms.module#FormsModule',
  },
  {
    path: 'manage-subscription',
    component: ManageSubscriptionComponent,
  },
   {
    path: 'tables',
    loadChildren: './tables/tables.module#TablesModule',
  },
 {
    path: 'miscellaneous',
    loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
  }, {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
