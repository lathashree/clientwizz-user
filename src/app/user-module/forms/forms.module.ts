import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { FormsRoutingModule, routedComponents } from './forms-routing.module';
import { NgDatepickerModule } from 'ng2-datepicker';
import { AngularFileUploaderModule } from 'angular-file-uploader';
import {ToasterModule} from 'angular2-toaster';

@NgModule({
  imports: [
    AngularFileUploaderModule,
    NgDatepickerModule,
    ThemeModule,
    FormsRoutingModule,
    ToasterModule.forRoot(),
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class FormsModule { }
