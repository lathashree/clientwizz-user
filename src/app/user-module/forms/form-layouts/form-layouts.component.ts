import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AuthService } from '../../../auth-service';
import { NgForm } from '@angular/forms';
import { DatepickerOptions } from 'ng2-datepicker';
import * as enLocale from 'date-fns/locale/en';
import {ToasterService} from 'angular2-toaster';



@Component({
  selector: 'ngx-form-layouts',
  styleUrls: ['./form-layouts.component.scss'],
  templateUrl: './form-layouts.component.html',
})

export class FormLayoutsComponent {
  afuConfig = {
    multiple: false,
    formatsAllowed: '.jpg, .png',
    maxSize: '2',
    theme: 'attachPin',
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: true,
    attachPinText: 'Upload photo'
};
  options: DatepickerOptions = {
    minYear: 1970,
    maxYear: 2030,
    displayFormat: 'MMM D[,] YYYY',
    barTitleFormat: 'MMMM YYYY',
    dayNamesFormat: 'dd',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    locale: enLocale,
    barTitleIfEmpty: 'Click to select a date',
    placeholder: 'Click to select a date', // HTML input placeholder attribute (default: '')
    addClass: 'form-control', // Optional, value to pass on to [ngClass] on the input field
    addStyle: {}, // Optional, value to pass to [ngStyle] on the input field
    fieldId: 'my-date-picker', // ID to assign to the input field. Defaults to datepicker-<counter>
    useEmptyBarTitle: false, // Defaults to true. If set to false then barTitleIfEmpty will be disregarded and a date will always be shown 
  };
  user
  startDate
  endDate
  detailsofclient

  progress: number;
  downloadUrl: string;
  uploadRunning: boolean;
  
  constructor(private authService: AuthService, private toaster: ToasterService){
    this.user = {}
  }




  ngOnInit(){
    this.authService.auth()
    .then((user:any) => {
      const ref = firebase.database().ref("/users");
      ref.child(user.uid).on("value", userSnapshot => {
        this.user.uid = user.uid;
        this.user.firstName = userSnapshot.val().firstName? userSnapshot.val().firstName:"";
        this.user.lastName = userSnapshot.val().lastName? userSnapshot.val().lastName: "";
        this.user.email = userSnapshot.val().email? userSnapshot.val().email: "";
        this.user.client = userSnapshot.val().client? userSnapshot.val().client:"";
        this.user.street = userSnapshot.val().street? userSnapshot.val().street: "";
        this.user.suite = userSnapshot.val().suite? userSnapshot.val().suite: "";
        this.user.city = userSnapshot.val().city? userSnapshot.val().city:"";
        this.user.state = userSnapshot.val().state? userSnapshot.val().state:"";
        this.user.ZIP = userSnapshot.val().ZIP? userSnapshot.val().ZIP:"";
        this.user.websiteUrl = userSnapshot.val().websiteUrl? userSnapshot.val().websiteUrl:"";
        this.user.linkedIn = userSnapshot.val().linkedIn? userSnapshot.val().linkedIn:"";
        this.user.toolsubscribed = userSnapshot.val().toolsubscribed? userSnapshot.val().toolsubscribed:"";
        this.user.accessLink = userSnapshot.val().accessLink? userSnapshot.val().accessLink:"";
        this.startDate = userSnapshot.val().startDate? userSnapshot.val().startDate:"";
        this.endDate = userSnapshot.val().endDate? userSnapshot.val().endDate:"";
        this.user.avatarUrl = userSnapshot.val().photoUrl
        console.log("user data", this.user)
      })

    });     
    this.uploadRunning = false;
    this.detailsofclient = []
    const cli = firebase.database().ref("/clients");    
    cli.on('value', userSnapshot => {
      let object = userSnapshot.val()
      for (const key in object) {
        if (object.hasOwnProperty(key)) {
         object[key].uid = key          
        }
      }
      this.detailsofclient = Object.values(object)
    });

  }


  captureFile(event) {
    console.log("capturing file event",{event})
    const file = event.target.files[0];
    this.saveFile(file);
  }

  saveFile(file) {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child('/user/'+this.user.uid+'/avatar/' + file.name).put(file);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
       (snapshot: any) => {
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        this.progress = progress;
        console.log('Upload is ' + progress + '% done');
        switch (snapshot.state) {
          case firebase.storage.TaskState.PAUSED: // or 'paused'
            this.uploadRunning = false;
            console.log('Upload is paused');
            break;
          case firebase.storage.TaskState.RUNNING: // or 'running'
            console.log('Upload is running');
            this.uploadRunning = true;
            break;
        }
      }, (error: any) => {

        // A full list of error codes is available at
        // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
          case 'storage/unauthorized':
            // User doesn't have permission to access the object
            break;

          case 'storage/canceled':
            // User canceled the upload
            break;

          case 'storage/unknown':
            // Unknown error occurred, inspect error.serverResponse
            break;
        }
      }, () => {
        // Upload completed successfully, now we can get the download URL
        uploadTask.snapshot.ref.getDownloadURL().then( (downloadURL) => {
          this.uploadRunning = false;
          this.downloadUrl = downloadURL;
          console.log('File available at', downloadURL);
          const ref = firebase.database().ref("/users")
          ref.child(this.user.uid).update({
            "photoUrl":this.downloadUrl
          })
        });
      });
  }

  onSubmit(form: NgForm) {
    const ref = firebase.database().ref("/users")
    if(this.startDate) form.value.startDate = this.startDate
    if(this.endDate) form.value.endDate = this.endDate
    ref.child(this.user.uid).update(form.value)
    this.toaster.pop('success', 'Your Profile has been updated', 'Successfully');
  }

}
