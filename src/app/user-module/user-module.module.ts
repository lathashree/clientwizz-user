import { NgModule } from '@angular/core';
import { AngularFileUploaderModule } from 'angular-file-uploader';

import { PagesComponent } from './user-module.component';
import { DashboardModule } from './dashboard/dashboard.module';
// import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './user-module-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { TokenRequestComponent } from './token-request/token-request.component';
import { ViewTokenComponent } from './view-token/view-token.component';
import { TokenComponent } from './token/token.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DownloadComponent } from './download/download.component';
import { ResolutionRenderComponent } from './token/resolution-render.component';
import { NmmDashboardComponent } from './nmm-dashboard/nmm-dashboard.component';
import { ManageSubscriptionComponent } from './manage-subscription/manage-subscription.component';
import {ToasterModule} from 'angular2-toaster';
import { UidRenderComponent } from './token/uid-render.component';
import { StatusRenderComponent } from './token/status-render.component';
import { ActionRenderComponent } from './download/action-render.component';
import { DownloadSecurekeyComponent } from './download-securekey/download-securekey.component';


const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    AngularFileUploaderModule,
    ThemeModule,
    DashboardModule,
    // ECommerceModule,
    MiscellaneousModule,
    Ng2SmartTableModule,
    ToasterModule.forRoot(),
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    TokenRequestComponent,
    ViewTokenComponent,
    TokenComponent,
    DownloadComponent,
    ResolutionRenderComponent,
    UidRenderComponent,
    StatusRenderComponent,
    ActionRenderComponent,
    NmmDashboardComponent,
    ManageSubscriptionComponent,
    DownloadSecurekeyComponent,
  ],
  entryComponents: [
    ResolutionRenderComponent,
    UidRenderComponent,
    StatusRenderComponent,
    ActionRenderComponent,
  ],
})
export class PagesModule {
}
