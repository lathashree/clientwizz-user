import { Component, Input, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AuthService } from '../../auth-service';
import { ViewCell } from 'ng2-smart-table';

import { Router, ActivatedRoute } from '@angular/router';

@Component({
  template: `
  <button *ngIf="!enableDownload" (click)="waveRequest()" [disabled]="requestDisabled" 
  class="btn btn-warning">Request</button><button *ngIf="enableDownload" (click)="waveDownload()" 
  [disabled]="requestDisabled" class="btn btn-success">Download</button>
  <span class="badge badge-pill badge-secondary" style="margin-left:20px;font-size:15px;">{{status}}</span>
  `,
})
export class ActionRenderComponent implements ViewCell, OnInit {

  status: string;
  requestDisabled: boolean
  enableDownload: boolean
  fileDownloaded: boolean


  @Input() value: any;
  @Input() rowData: any;

  constructor(private authService: AuthService, private router: ActivatedRoute, private route: Router) {}

  ngOnInit() {
    this.status = this.value
    console.log({status:this.status})
    if(this.status === 'Request Pending'){
      this.requestDisabled = true
    }else if(this.status === 'Request Approved'){
      this.enableDownload = true
    }
    else if(this.status === 'Completed'){
      this.fileDownloaded = true
    }
    return true
  }

  waveRequest(){
    let userId;
    console.log("wave request",this.rowData)
    this.authService.auth()
    .then((user: any) => {
       userId = user.uid;
       const ref = firebase.database().ref('/users/' + userId + '/wavedataStatus');
       var wavedatastatus = 'Request Pending';
       ref.child(this.rowData.uid).update({
         'status': wavedatastatus,
       });
       this.requestDisabled = true
    });
  }

  waveDownload(){    
    console.log("attachment rowdata",this.rowData)
    this.router.params.subscribe(params => {
      this.route.navigate(['/user/download-securekey/'+this.rowData.uid])
    })

    let userId;
    console.log("wave request",this.rowData)
    this.authService.auth()
    .then((user: any) => {
       userId = user.uid;
       const ref = firebase.database().ref('/users/' + userId + '/wavedataStatus');
       var wavedatastatus = '';
       ref.child(this.rowData.uid).update({
         'status': wavedatastatus,
       });
       this.fileDownloaded = true
       this.requestDisabled = false
       this.enableDownload = false
       
    });
  }


}
