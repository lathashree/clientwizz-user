import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AuthService } from '../../auth-service';
import { LocalDataSource } from 'ng2-smart-table';
import {ActionRenderComponent} from './action-render.component';

@Component({
  selector: 'ngx-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss'],
})
export class DownloadComponent implements OnInit {

  settings = {

    actions: {
      add : false,
      edit: false,
      delete: false,
      position : "right",
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },

    columns: {
      wavePeriod: {
        title: 'Wave Period',
        type: 'string',
      },
      description: {
        title: 'Description',
        type: 'string',
      },
      status: {
        title: 'Action',
        type: 'custom',
        renderComponent: ActionRenderComponent,
      },
 
    },
  };
  

  source: LocalDataSource = new LocalDataSource();
  wavedataArr


  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.wavedataArr = []

    this.authService.auth()  
    .then((user: any) => {
       const ref = firebase.database().ref('/users/' + user.uid + '/wavedataStatus');
       ref.once('value', userSnapshot => {
        let object = userSnapshot.val()
        const waveRef = firebase.database().ref("/waveData")
        waveRef.once('value', wavedataset => {
          let waveObject = wavedataset.val()
            for(const key in waveObject){
              if (waveObject.hasOwnProperty(key)){
                if(object && object[key]){
                  let status = object[key].status
                  waveObject[key].status = status
                }
                waveObject[key].uid = key;
                this.wavedataArr.push(waveObject[key]);
              }
            }
            this.source.load(this.wavedataArr);
            // console.log({wavedataArr: this.wavedataArr})   
        });
      });
     
    });

    
  }

}
