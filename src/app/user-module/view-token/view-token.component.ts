import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import * as firebase from 'firebase';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth-service';
import { NgForm } from '@angular/forms';
import {ToasterService} from 'angular2-toaster';

@Component({
  selector: 'ngx-view-token',
  templateUrl: './view-token.component.html',
  styleUrls: ['./view-token.component.scss']
})
export class ViewTokenComponent implements OnInit {

  source: LocalDataSource = new LocalDataSource();
  uid
  messages 
  tokenId
  messageStatus
  constructor(private router: ActivatedRoute, private route: Router, private authService: AuthService, private toaster: ToasterService) { }

  ngOnInit() {
    this.messages = []
    this.authService.auth()

      .then((user: any) => {
        this.uid = user.uid;
     
        const ref = firebase.database().ref("/users")
        this.router.params.subscribe(params => {
          this.tokenId = params.id
          ref.child(this.uid + "/supportTickets/" + params.id).on('value', messagesSnapshot => {
            let object = messagesSnapshot.val()
            this.messageStatus = object.status;

            console.log("object",this.messageStatus)
            object.id = params.id;

            let messages = []
            messages.push({
              text: object.description,
              time: object.time,
            })
            if(!object.messages) object.messages = []
            this.messages = messages.concat(Object.values(object.messages));
          });
        });
      });

  }

  onSubmit(form: NgForm){
    if(form.value.text.trim().length == 0){
      this.toaster.pop('error', 'Your message is empty', 'Error');
      return
    }
    const ref = firebase.database().ref("/users/"+this.uid+"/supportTickets/"+this.tokenId+"/messages")
    const newviewtoken = ref.push()
    form.value.time = new Date().toDateString();
    newviewtoken.set(form.value);
  }

}
