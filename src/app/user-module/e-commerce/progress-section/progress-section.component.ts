import {Component} from '@angular/core';

@Component({
  selector: 'ngx-progress-section',
  styleUrls: ['./progress-section.component.scss'],
  templateUrl: './progress-section.component.html',
})
export class ECommerceProgressSectionComponent {
  progressInfoData = [
    {
      title: 'All users',
      value: 572900,
      activeProgress: 70,
      description: 'Count of users registered overall',
    },
    {
      title: 'New Users',
      value: 6378,
      activeProgress: 30,
      description: 'Count of users registered in last 7 days',
    },
    {
      title: 'Recent Users',
      value: 200,
      activeProgress: 55,
      description: 'Count of users signedin in last 7 days',
    },
  ];
}
