import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import {PeriodsService} from '../../../@core/data/periods.service'
import { SmartTableService } from '../../../@core/data/smart-table.service';

@Component({
  selector: 'ngx-smart-table',
  templateUrl: './smart-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class SmartTableComponent {

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      fullName: {
        title: 'Full Name',
        type: 'string',
      },
      username: {
        title: 'Username',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      report: {
        title: 'Report',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableService, private periods: PeriodsService) {
    let data: any = this.service.getData();
    data = data.map(obj=>{
      obj.fullName = obj.firstName+' '+obj.lastName;
      const reports = this.periods.getReports();
      if (obj.report < 20){
        obj.report = reports[0]
      }else if(obj.report > 20 && obj.report < 30){
        obj.report = reports[1]
      }else if(obj.report > 30 && obj.report < 40){
        obj.report = reports[2]
      }else{
        obj.report = reports[3]
      }
      return obj
    })
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}