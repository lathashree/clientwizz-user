import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TablesComponent } from './tables.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { TransactionsComponents } from './nmm-data/transactions.component';

const routes: Routes = [{
  path: '',
  component: TablesComponent,
  children: [
  {
    path: 'users-table',
    component: SmartTableComponent,
  },
  {
    path: 'nmm-data',
    component: TransactionsComponents,
  },
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TablesRoutingModule { }

export const routedComponents = [
  TablesComponent,
  SmartTableComponent,
  TransactionsComponents,
];
