import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../auth-service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-token-request',
  templateUrl: './token-request.component.html',
  styleUrls: ['./token-request.component.scss'],
})
export class TokenRequestComponent implements OnInit {
  products: Array<any> = [
    { name: 'Devops cloud setup', categories: ['New installation', 'Access details']},
    { name: 'IOT Managed Dashboard', categories: ['Support Ticket', 'Feedback']},
    { name: 'Blockchain as a service', categories: ['Complaint', 'Others']},
    
  ];
  categories: Array<any>;
  changecategory(count) {
    this.categories = this.products.find(con => con.name == count).categories;
  }



  afuConfig = {
    multiple: false,
    formatsAllowed: '.jpg, .png',
    theme: 'attachPin',
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: true,
  };
  userId;
  progress: number;
  downloadUrl: string;
  uploadRunning: boolean;
  constructor(private authService: AuthService, private router: Router) {
      
     
   }

  ngOnInit() {
    this.authService.auth()
      .then((user: any) => {
        this.userId = user.uid;
      });
      this.uploadRunning = false;
  }

  captureFile(event) {
    console.log("capturing file event",{event})
    const file = event.target.files[0];
    this.saveFile(file);
  }

  saveFile(file) {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child('images/' + file.name).put(file);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
       (snapshot: any) => {
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        this.progress = progress;
        console.log('Upload is ' + progress + '% done');
        switch (snapshot.state) {
          case firebase.storage.TaskState.PAUSED: // or 'paused'
            this.uploadRunning = false;
            console.log('Upload is paused');
            break;
          case firebase.storage.TaskState.RUNNING: // or 'running'
            console.log('Upload is running');
            this.uploadRunning = true;
            break;
        }
      }, (error: any) => {

        // A full list of error codes is available at
        // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
          case 'storage/unauthorized':
            // User doesn't have permission to access the object
            break;

          case 'storage/canceled':
            // User canceled the upload
            break;

          case 'storage/unknown':
            // Unknown error occurred, inspect error.serverResponse
            break;
        }
      }, () => {
        // Upload completed successfully, now we can get the download URL
        uploadTask.snapshot.ref.getDownloadURL().then( (downloadURL) => {
          this.uploadRunning = false;
          this.downloadUrl = downloadURL;
          console.log('File available at', downloadURL);
        });
      });
  }

  onSubmit(form: NgForm) {
    console.log("SS console",form.value)
    const ref = firebase.database().ref('/users/' + this.userId + '/supportTickets');
    const newsupportSystem = ref.push();
    form.value.time = new Date().toDateString();
    form.value.status = 'Pending';
    if (form.value.attachment) form.value.attachment = this.downloadUrl;
    newsupportSystem.set(form.value);
    this.router.navigate(['/user/token']);
  }

  removeattachment(){
    console.log("remove")
    this.downloadUrl = '';
  }


}
