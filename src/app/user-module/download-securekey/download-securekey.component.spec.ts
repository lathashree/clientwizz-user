import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadSecurekeyComponent } from './download-securekey.component';

describe('DownloadSecurekeyComponent', () => {
  let component: DownloadSecurekeyComponent;
  let fixture: ComponentFixture<DownloadSecurekeyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadSecurekeyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadSecurekeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
