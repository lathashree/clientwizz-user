import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute  } from '@angular/router';
import * as firebase from 'firebase';
import { AuthService } from '../../auth-service';

@Component({
  selector: 'ngx-download-securekey',
  templateUrl: './download-securekey.component.html',
  styleUrls: ['./download-securekey.component.scss']
})
export class DownloadSecurekeyComponent implements OnInit {
  attachment
  public text: string = 'Download';

  settings = {


    columns: {
      attachemnt: {
        // title: 'Wave Period',
        type: 'string',
      },

    },
  };

  constructor(private route: Router, private router: ActivatedRoute, private authService: AuthService) { }

  ngOnInit() {
    this.router.params.subscribe(params => {
      const ref = firebase.database().ref("/waveData")
      ref.child(params.id).on("value", userSnapshot =>{
        this.attachment = userSnapshot.val().attachment
        console.log("ngoninit",userSnapshot.val().attachment)
      })

    });

  }

  public back(): void {
    if(this.text === 'Download') {
      this.text = 'Back'
    } else {
      this.text = 'Download'
    }
  }



  onSubmit(form: NgForm) {
    const securekey = form.value.securekey;
            this.route.navigate(['/user/download'])
}

}
