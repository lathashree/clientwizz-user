import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  // {
  //   title: 'NMM Tracking Dashboard',
  //   icon: 'nb-layout-sidebar-right',
  //   link: '/user/dashboard',
  //   home: true,
  // },
  {
    title: 'My Account',
    icon: 'nb-gear',
    link: '/user/forms/layouts',
  },
  // {
  //   title: 'Subscription Status',
  //   icon: 'nb-bar-chart',
  //   link: '/user/manage-subscription',
  // },
  {
    title: 'Support System',
    icon: 'nb-compose',
    link: '/user/token',
    pathMatch: '/user/token/*',
  },
  // {
  //   title: 'Download NMM Data',
  //   icon: 'nb-arrow-thin-down',
  //   link: '/user/download',
  // },
  {
    title: 'Logout',
    icon: 'nb-power',
    link: '/auth/logout',
  },
];
